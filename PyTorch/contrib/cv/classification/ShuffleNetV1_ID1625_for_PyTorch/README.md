# ShuffleNetV1 for PyTorch

-   [概述](概述.md)
-   [准备训练环境](准备训练环境.md)
-   [开始训练](开始训练.md)
-   [训练结果展示](训练结果展示.md)
-   [版本说明](版本说明.md)



# 概述

## 简述

ShuffleNet V1是一个计算效率极高的图像分类网络，它是专门为计算能力非常有限的移动设备设计的（例如，10-150 MFLOPs）。其采用了两种新的操作，逐点分组卷积（Pointwise Group Convolution）和通道重排（channel shuffle），在保持精度的同时大大降低了计算成本。
- 参考实现：

  ```
  url=https://github.com/megvii-model/ShuffleNet-Series/tree/master/ShuffleNetV1
  commit_id=d69403d4b5fb3043c7c0da3c2a15df8c5e520d89  
  ```

- 适配昇腾 AI 处理器的实现：

  ```
  url=https://gitee.com/ascend/ModelZoo-PyTorch.git
  code_path=PyTorch/contrib/cv/classification
  ```
  
- 通过Git获取代码方法如下：

  ```
  git clone {url}       # 克隆仓库的代码
  cd {code_path}        # 切换到模型代码所在路径，若仓库下只有该模型，则无需切换
  ```
  
- 通过单击“立即下载”，下载源码包。

# 准备训练环境

## 准备环境

- 当前模型支持的固件与驱动、 CANN 以及 PyTorch 如下表所示。

  **表 1**  版本配套表

  | 配套       | 版本                                                         |
  | ---------- | ------------------------------------------------------------ |
  | 固件与驱动 | [1.0.17](https://www.hiascend.com/hardware/firmware-drivers?tag=commercial) |
  | CANN       | [6.0.RC1](https://www.hiascend.com/software/cann/commercial?version=6.0.RC1) |
  | PyTorch    | [1.8.1](https://gitee.com/ascend/pytorch/tree/master/) |

- 环境准备指导。

  请参考《[Pytorch框架训练环境准备](https://www.hiascend.com/document/detail/zh/ModelZoo/pytorchframework/ptes)》。
  
- 安装依赖。

  ```
  pip install -r requirements.txt
  ```


## 准备数据集

1. 获取数据集。

   用户自行获取原始数据集，可选用的开源数据集包括ImageNet2012，CIFAR-10等，将数据集上传到服务器任意路径下并解压。

   以ImageNet2012数据集为例，数据集目录结构参考如下所示。

   ```
   ├── ImageNet2012
         ├──train
              ├──类别1
                    │──图片1
                    │──图片2
                    │   ...       
              ├──类别2
                    │──图片1
                    │──图片2
                    │   ...   
              ├──...                     
         ├──val  
              ├──类别1
                    │──图片1
                    │──图片2
                    │   ...       
              ├──类别2
                    │──图片1
                    │──图片2
                    │   ...              
   ```

   > **说明：** 
   >该数据集的训练过程脚本只作为一种参考示例。



# 开始训练

## 训练模型

1. 进入解压后的源码包根目录。

   ```
   cd /${模型文件夹名称} 
   ```

2. 运行训练脚本。

   该模型支持单机单卡训练和单机8卡训练。

   - 单机单卡训练

     启动单卡训练。

     ```
     bash ./test/train_full_1p.sh --data_path=/data/xxx/         # 精度训练
     bash ./test/train_performance_1p.sh --data_path=/data/xxx/  # 性能训练
     ```

   - 单机8卡训练

     启动8卡训练。

     ```
     bash ./test/train_full_8p.sh --data_path=/data/xxx/         # 精度训练
     bash ./test/train_performance_8p.sh --data_path=/data/xxx/  # 性能训练   
     ```

   --data\_path参数填写数据集路径。

   模型训练脚本参数说明如下。

   ```
   公共参数：
   --data                              //数据集路径
   --addr                              //主机地址
   --workers                           //加载数据进程数      
   --epochs                            //重复训练次数
   --batch-size                        //训练批次大小，默认：240
   --learning-rate                     //初始学习率，默认：1
   --momentum                          //动量，默认：0.9
   --weight_decay                      //权重衰减，默认：4e-5
   --amp                               //是否使用混合精度
   --loss-scale                        //混合精度lossscale大小
   --opt-level                         //混合精度类型，默认：O2
   多卡训练参数：
   --distributed                       //是否使用多卡训练
   ```
   
   训练完成后，权重文件保存在当前路径下，并输出模型训练精度和性能信息。

# 训练结果展示

**表 2**  训练结果展示表

| NAME    | Acc@1 |  FPS | Npu nums | Epochs | AMP_Type |
| ------- | ----- | ---: | ------   | ------ | -------: |
| NPU_1.5 | 67.21 |   462|        1 |    240 |       O2 |
| NPU_1.8 | -     |2912.3|        1 |    240 |       O2 |
| NPU_1.5 | 66.45 |  3956|        8 |    240 |       O2 |
| NPU_1.8 | 66.30 | 14510|        8 |    240 |       O2 |


# 版本说明

## 变更

2022.09.26：更新torch版本1.8，重新发布。

2020.07.08：首次发布。

## 已知问题

无。

